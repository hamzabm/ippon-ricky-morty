package com.example.rickandmortyapp.util

import com.example.rickandmortyapp.R
import junit.framework.TestCase

class ColorUtilTest : TestCase() {

    fun testGetCharacterColor() {
        assertEquals(R.color.lightTeal,ColorUtil(null).getCharacterColor("human"))
        assertEquals(R.color.lightRed,ColorUtil(null).getCharacterColor("humanoid"))
        assertEquals(R.color.lightBlue,ColorUtil(null).getCharacterColor("animal"))
        assertEquals(R.color.lightYellow,ColorUtil(null).getCharacterColor("alien"))
        assertEquals(R.color.lightYellow,ColorUtil(null).getCharacterColor("vampire"))
        assertEquals(R.color.lightPurple,ColorUtil(null).getCharacterColor("poopybutthole"))
        assertEquals(R.color.lightBrown,ColorUtil(null).getCharacterColor("mytholog"))
    }

}