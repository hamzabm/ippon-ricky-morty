package com.example.rickandmortyapp

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.rickandmortyapp.data.local.Database
import com.example.rickandmortyapp.data.local.dao.CharacterDao
import com.example.rickandmortyapp.data.local.dao.EpisodeDao
import com.example.rickandmortyapp.data.model.Character
import com.example.rickandmortyapp.data.model.Episode
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class DateBaseTests {
    private lateinit var charactereDao: CharacterDao
    private lateinit var episodeDao: EpisodeDao
    private lateinit var db: Database

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, Database::class.java).build()
        charactereDao = db.getCharacterDao()
        episodeDao=db.getEpisodeDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun characterDbTest() {
        val character = Character()
        character.id = "1"
        character.name = "Hamza"
        charactereDao.addCharacter(character)
        val byName = charactereDao.getAllCharactersForTest()
        assertThat(byName[0], equalTo(character))
    }
    @Test
    @Throws(Exception::class)
    fun episodeDbTest() {
        val ep = Episode(0,"2020","Episode 1","1","1","EP 1")
        episodeDao.addEpisodes(listOf(ep))
        val byName = episodeDao.getEpisodesPerCharacterForTest("1")
        assertThat(byName.get(0).characterId, equalTo(ep.characterId))
    }
}