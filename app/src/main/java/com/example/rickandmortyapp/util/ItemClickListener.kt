package com.example.rickandmortyapp.util

import android.view.View
import com.example.rickandmortyapp.data.model.Character

interface ItemClickListener {
    fun onClick(character:Character,view:View)
}