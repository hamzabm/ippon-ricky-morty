package com.example.rickandmortyapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.rickandmortyapp.data.model.Character
import com.example.rickandmortyapp.data.model.Episode
import retrofit2.http.DELETE

@Dao
interface EpisodeDao:BaseDao<Character> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
     fun addEpisodes(episodes:List<Episode>)

    @Query("DELETE FROM episodes")
     fun clear()

    @Query("SELECT * FROM episodes WHERE character_id=:characterId")
    fun getEpisodesPerCharacter(characterId:String):LiveData<List<Episode>>

    @Query("SELECT * FROM episodes WHERE character_id=:characterId")
    fun getEpisodesPerCharacterForTest(characterId:String):List<Episode>
}